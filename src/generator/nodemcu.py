from collections import OrderedDict

from jinja2 import Environment, FileSystemLoader
import os
import sys
import errno
import inspect
from copy import copy, deepcopy
from model.parameter import ParameterType
from model.one_wire_address import OneWireAddress, ONE_WIRE_EMPTY_ADDRESS
from model.enums import Status, WifiMode
from model.generic_controller import ContactSensorPin, OneWirePin
from model.rules_engine import TranslateParamValueRule, SendParamToUrlRule, PredefinedRulesEngine
from model.relay import Relay


class NodeMcuFirmwareGenerator:
    wifi_modes_mapping = {WifiMode.disabled:      WifiMode.disabled.value,       # WIFI_OFF
                          WifiMode.access_point:  WifiMode.access_point.value,   # WIFI_AP
                          WifiMode.static_client: WifiMode.static_client.value,  # WIFI_STA
                          WifiMode.dhcp_client:   WifiMode.dhcp_client.value     # do not set any particular mode
                          }

    def __init__(self):
        pass

    def generate_firmware(self, nodemcu_model, target_file=None, template_file=None):

        env = Environment(loader=FileSystemLoader('%s/templates/' % os.path.dirname(__file__)),
                          trim_blocks=True, lstrip_blocks=True)
        if template_file is None:
            template = env.get_template('nodemcu.ino')
        else:
            template = env.get_template(template_file)

        if not os.path.exists(os.path.dirname(target_file)):
            try:
                os.makedirs(os.path.dirname(target_file))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise

        self.validate_model(nodemcu_model)
        expanded_model = self.expand_model_with_precalculated_data(nodemcu_model)
        context_variables = self.extract_context_variables(expanded_model)
        self.import_libraries(context_variables)

        with open(target_file, "wb") as target:
            target.write(template.render(context_variables).encode('UTF-8'))

        return template.render(context_variables)

    def extract_context_variables(self, nodemcu_model):
        # use all nodemcu properties to rendering parameters dict
        variables_dict = copy(nodemcu_model.__dict__)

        # find all property-like methods (use_logging, etc.) read them and put to parameters dict
        for class_attr_name in dir(nodemcu_model):
            class_attr = getattr(nodemcu_model, class_attr_name)
            if inspect.ismethod(class_attr) and not class_attr_name.startswith('_'):
                method = class_attr
                args_spec = inspect.getfullargspec(method)
                if len(args_spec.args) == 1 and args_spec.args[0] == 'self':
                    variables_dict[class_attr_name] = method()

        # add all param C values to the variables_dict
        for param in nodemcu_model.params.values():
            variables_dict[param.id] = param.cvalue

        # add pre calculated values
        variables_dict['max_param_cid_length'] = str(max([len(x.cid) for x in variables_dict['params'].values()]))
        variables_dict['params_sorted_by_id'] = sorted(variables_dict['params'].values(), key=lambda x: x.num_id)
        variables_dict['wifi_modes_mapping'] = self.wifi_modes_mapping

        return variables_dict

    def validate_model(self, nodemcu_model):
        # all devices should have unique IDs
        for device in nodemcu_model.devices:
            if device.id is None:
                raise ValueError('device ID is not initialized. Device = {}, device.name = {}'.format(device, device.name))
            conflict_devices = [device2 for device2 in nodemcu_model.devices if device2.id == device.id and device2 is not device]
            if len(conflict_devices) > 0:
                raise ValueError('device ID {} is not unique. Conflict device names: {}'
                                 .format(device.id, [device.name] + [device.name for device in conflict_devices]))

        # all pins should have a unique number and combination of pin.type + pin.id should be unique as well
        for pin in nodemcu_model.pins:
            if pin.pin_number is None:
                raise ValueError('pin number is not initialized. Pin = {}, pin.id = {}'.format(pin, pin.id))
            conflict_pins = [pin2 for pin2 in nodemcu_model.pins
                             if (pin2.pin_number == pin.pin_number or (pin2.type == pin.type and pin2.id == pin.id)) and pin2 is not pin]
            if len(conflict_pins) > 0:
                raise ValueError('there are conflicting pins [{}]'.format(', '.join([str(pin.__dict__) for pin in [pin] + conflict_pins])))

        # all contact sensor devices must have a pin assigned and this pin must belong to ContactSensorPin class
        for device in nodemcu_model.contact_sensors():
            if device.pin is None:
                raise ValueError('Contact sensor "{}" has no pin assigned.'.format(device.name))
            if not isinstance(device.pin, ContactSensorPin):
                raise ValueError('Contact sensor "{}" pin class is {}. Only ContactSensorPin is allowed.'.format(device.name))

        # validate rules
        for rule in nodemcu_model.rules_engine.rules:
            if isinstance(rule, TranslateParamValueRule):
                if rule.source_param.type != rule.target_param.type:
                    raise ValueError('TranslateParamValueRule non compatible source_param and target_param types. '
                                     'Source param: {}, target param: {}'.format(rule.source_param.type, rule.target_param.type))
            elif isinstance(rule, SendParamToUrlRule):
                if nodemcu_model.http_client is None:
                    raise ValueError('SendParamToUrlRule is used for nodemcu model with http_client switched off. '
                                     'Rule params: {}, target URL: {}'.format(rule.params, rule.url))
            else:
                raise ValueError('unsupported Rule type {}'.format(type(rule)))

        # validate wifi mode
        if nodemcu_model.wifi_mode not in self.wifi_modes_mapping.keys():
            raise ValueError('unmapped wifi mode value {}. See generator implementation for details.'.format(nodemcu_model.wifi_mode))
        # validate mdns
        if nodemcu_model.use_mdns and nodemcu_model.mdns_name is None or len(nodemcu_model.mdns_name) <= 0:
            raise ValueError('incorrect mdns_name value {}. Please, set not empty String value to mdns_name variable or switch use_mdns flag to false.'.format(nodemcu_model.mdns_name))
        # validate network features compatibility
        network_features = {'use_web_server': nodemcu_model.use_web_server(),
                            'use_mdns': nodemcu_model.use_mdns()}
        if not nodemcu_model.use_networking() and any(network_features.values()):
            raise ValueError('use_networking is False but some of network features are enabled: {}'.format([key for key in network_features.keys() if network_features[key]]))

    def expand_model_with_precalculated_data(self, nodemcu_model):
        expanded_model = deepcopy(nodemcu_model)
        params = OrderedDict()
        for device in [nodemcu_model] + expanded_model.devices:
            for param_id, param_value in device.params.items():
                params[param_id] = param_value
        expanded_model.params = params

        for param in params.values():
            # add C #define param ID
            param.cid = 'ID_{}'.format(param.id.upper())  # parameter numerical ID (for HTTP API)
            param.cvar = param.id  # name of C variable where the param value stored

            # add C equivalent of parameter type
            if param.type == ParameterType.boolean:
                ctype = 'byte'
            elif param.type == ParameterType.byte:
                ctype = 'byte'
            elif param.type == ParameterType.word:
                ctype = 'word'
            elif param.type == ParameterType.int:
                ctype = 'int'
            elif param.type == ParameterType.float:
                ctype = 'float'
            elif param.type == ParameterType.long:
                ctype = 'long'
            elif param.type == ParameterType.status:
                ctype = 'byte'
            elif param.type == ParameterType.wifi_mode:
                ctype = 'byte'
            elif param.type == ParameterType.switch_state:
                ctype = 'byte'
            elif param.type == ParameterType.open_state:
                ctype = 'byte'
            elif param.type == ParameterType.string:
                ctype = 'String'
            elif param.type == ParameterType.ip_address:
                ctype = 'IPAddress'
            else:
                ctype = param.type.name
            param.ctype = ctype  # C equivalent of parameter value type

            # add C equivalent of parameter value
            value = param.read_value()
            if param.type == ParameterType.switch_state:
                cvalue = 'ON' if value else 'OFF'
            elif param.type == ParameterType.open_state:
                cvalue = 'OPEN' if value else 'CLOSED'
            elif param.type == ParameterType.boolean:
                cvalue = 1 if value else 0
            elif param.type == ParameterType.float:
                # cvalue = '{0:.2f}'.format(param_value)
                cvalue = value
            elif param.type == ParameterType.string:
                cvalue = 'String("{}")'.format(value)
            elif param.type == ParameterType.status:
                cvalue = 'STATUS_{}'.format(value.name.upper())
            elif param.type == ParameterType.wifi_mode:
                cvalue = 'WIFI_MODE_{}'.format(value.name.upper())
            elif param.type in [ParameterType.one_wire_address, ParameterType.ip_address]:
                cvalue = value.value
            else:
                cvalue = value
            param.cvalue = cvalue

        for pin in expanded_model.pins:
            pin.cid = 'PIN_' + pin.type.upper() + ('_' + pin.id.upper() if pin.id is not None else '')
            if expanded_model.use_debounce_lib() and isinstance(pin, ContactSensorPin):
                if pin.id is not None:
                    pin.debounce_var_name = '{}_debounce'.format(pin.id.lower())
                else:
                    pin.debounce_var_name = 'pin_{}_debounce'.format(pin.pin_number.lower())
            if expanded_model.use_one_wire_lib() and isinstance(pin, OneWirePin):
                if len(expanded_model.one_wire_pins()) == 1:
                    pin.one_wire_var_name = 'one_wire'
                elif pin.id is not None:
                    pin.one_wire_var_name = 'one_wire_{}'.format(pin.id.lower())
                else:
                    pin.one_wire_var_name = 'one_wire_{}'.format(pin.pin_number.lower())

        return expanded_model

    def import_libraries(self, context_variables):
        context_variables['list'] = list
        context_variables['str'] = str
        context_variables['hex'] = hex
        context_variables['map'] = map
        context_variables['len'] = len
        context_variables['isinstance'] = isinstance
        context_variables['is_simple_param_type'] = is_simple_param_type
        context_variables['ONE_WIRE_EMPTY_ADDRESS'] = ONE_WIRE_EMPTY_ADDRESS

        # add all imported classes to Jinja context
        imported_classes = inspect.getmembers(sys.modules[__name__], inspect.isclass)
        for imported_class in imported_classes:
            context_variables[imported_class[0]] = imported_class[1]


def to_hex(value, minimum_hex_digits=4):
    return '{0:#0{1}x}'.format(value, minimum_hex_digits)


def to_byte_array(value, minimum_hex_digits=4):
    return '{0:#0{1}x}'.format(value, minimum_hex_digits)


def is_simple_param_type(param_type):
    """ Detect if the parameter value can be assignable with simple "=" operator. """
    return param_type in [ParameterType.boolean,
                          ParameterType.byte,
                          ParameterType.word,
                          ParameterType.int,
                          ParameterType.long,
                          ParameterType.float,
                          ParameterType.status,
                          ParameterType.switch_state,
                          ParameterType.open_state,
                          ParameterType.wifi_mode]

