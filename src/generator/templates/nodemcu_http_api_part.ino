
// ===========================================================================
// =====                     HTTP API section                            =====
// ===========================================================================

void initWebServer() {
  webServer.on("/", HTTP_GET, onGet);
  webServer.on("/", HTTP_POST, onPost);
  webServer.on("/reboot", HTTP_GET, doReset);
  webServer.begin();
}

void handleNetworkService() {
  webServer.handleClient();
}

void onPost() {
  log(webServer.args());
  log("Handle POST request");

  for (byte i = 0; i < webServer.args(); i++) {
    String paramStr = webServer.argName(i);
    char* errorMsg = 0;
    unsigned int paramId = parseParamIdStr(paramStr.c_str(), paramStr.length(), errorMsg);
    if (errorMsg != 0) {
      String response = "Unable to parse param ID: paramId=\"";
      response.concat(paramStr);
      response.concat("\", errorMsg=\"");
      response.concat(errorMsg);
      response.concat("\"");
      webServer.send(400, "text/html", response);
    }
    // process parameter by ID
    paramStr = webServer.arg(i);
    errorMsg = 0;
      void* paramValue = parseParamStrValue(paramId, paramStr.c_str(), paramStr.length(), errorMsg);
      if (errorMsg != 0) {
        String response = "Unable to parse param value: paramId=";
        response.concat(paramId);
        response.concat(", paramValueStr=\"");
        response.concat(paramStr);
        response.concat("\", errorMsg=\"");
        response.concat(errorMsg);
        response.concat("\"");
        webServer.send(400, "text/html", response);
      } else {
        setParamValue(paramId, paramValue);
      }
  }
  webServer.send(200, "text/html", "done");
}

void onGet() {
  log("onGet: ");
  logln(webServer.uri());
  String response = "";
{% for param in params.values() %}
  response.concat("{{ param.id }} ({{ param.num_id }}) = ");
  response.concat(paramValueToStr({{ param.cid }}));
  response.concat("\n");
{% endfor %}
  webServer.send(200, "text/html", response);
}
