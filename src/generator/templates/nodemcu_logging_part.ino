// ===========================================================================
// =====                     Logging section                             =====
// ===========================================================================
{% if use_network_logging %}
EthernetClient log_client;    // client connection to LogServer socket.
IPAddress LOG_SERVER_HOST   ({{ ", ".join(log_server_ip_address.value) }});
int LOG_SERVER_PORT = {{ log_server_port }};

{% endif %}
void log(String msg) {
{% if use_serial_logging %}
  Serial.print(msg);
{% endif %}
{% if use_network_logging %}
  if (config_debug_network_output_state) {
    sendLogOverNetwork(msg);
  }
{% endif %}
{% if not use_logging %}
  // logging disabled
{% endif %}
}

void logln(String msg) {
{% if use_serial_logging %}
  Serial.println(msg);
{% endif %}
{% if use_network_logging %}
  if (config_debug_network_output_state) {
    sendLogLnOverNetwork(msg);
  }
{% endif %}
{% if not use_logging %}
  // logging disabled
{% endif %}
}

void log(int value) {
{% if use_logging %}
  String str = String(value);
  log(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(int value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void log(const char* value) {
{% if use_logging %}
  String str = String(value);
  log(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(const char* value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void log(bool value) {
{% if use_logging %}
  String str = String(value);
  log(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(bool value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void log(float value, int deciamalDigits) {
{% if use_logging %}
  String str = String(value, deciamalDigits);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(float value, int deciamalDigits) {
{% if use_logging %}
  String str = String(value, deciamalDigits);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void log(unsigned long value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(unsigned long value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

{% if use_networking %}
void log(IPAddress value) {
{% if use_logging %}
  String str = String(value);
  log(str);
{% else %}
  // logging disabled
{% endif %}
}

void logln(IPAddress value) {
{% if use_logging %}
  String str = String(value);
  logln(str);
{% else %}
  // logging disabled
{% endif %}
}

{% endif %}
{% if use_network_logging %}
bool sendLogOverNetworkInner(String msg) {
  if (status_log_server_connection_state == STATUS_FAIL) {
    // Serial.println("Network Log sending is skipped because of failed connection status.");
  }
  else if (log_client.connected() || log_client.connect(LOG_SERVER_HOST, LOG_SERVER_PORT)) {
    for (int i = 0; i < msg.length(); i++) {
      log_client.write(msg[i]);
    }
    return true;
  }
  else {
{% if use_network_logging %}
    Serial.println("log server: connection failed");
{% endif %}
    log_client.stop();
    updateNetworkConnectionStatus({{ 'ID_' + params.get('log_server_connection_status').name.upper() }}, STATUS_FAIL, false);
  }
  return false;
}

void sendLogOverNetwork(String msg) {
  if (sendLogOverNetworkInner(msg)) {
    log_client.flush();
  }
}

void sendLogLnOverNetwork(String msg) {
  if (sendLogOverNetworkInner(msg)) {
    log_client.write('\n');
    log_client.flush();
  }
}

{% endif %}
{% if use_one_wire_lib %}
void logOneWireAddress(uint8_t* address) {
{% if use_logging %}
  if (address != ONE_WIRE_EMPTY_ADDRESS) {
    log("{ 0x");
    log(String(address[0], HEX));
    for (int i = 1; i < 8; i++) {
      log(", 0x");
      log(String(address[i], HEX));
    }
    log(" }");
  } else {
    log("ONE_WIRE_EMPTY_ADDRESS");
  }
{% else %}
  // logging disabled
{% endif %}
}

{% endif %}
{% if use_networking %}
//void logIPAddress(uint8_t* address) {
//{% if use_logging %}
//  for (int i = 2; i < 6; i++) {
//    if (i > 2) {
//      log(".");
//    }
//    log(address[i]);
//  }
//{% else %}
//  // logging disabled
//{% endif %}
//}

{% endif %}
{% if use_dht_wire_lib %}
void logDHTSensorSuccess() {
{% if use_logging %}
  log("sensor read OK. temperature = ");
  log(DHT.temperature, 2);
  log(", humidity = ");
  logln(DHT.humidity, 2);
{% else %}
  // logging disabled
{% endif %}
}

void logDHTSensorError(int result) {
{% if use_logging %}
  switch (result) {
    case DHTLIB_ERROR_CHECKSUM:
      logln("sensor checksum error.");
      break;
    case DHTLIB_ERROR_TIMEOUT:
      logln("sensor time out error.");
      break;
    case DHTLIB_ERROR_CONNECT:
      logln("sensor connection error.");
      break;
    case DHTLIB_ERROR_ACK_L:
      logln("sensor Ack Low error.");
      break;
    case DHTLIB_ERROR_ACK_H:
      logln("sensor Ack High error.");
      break;
    default:
      logln("sensor unknown error.");
      break;
  }
{% else %}
  // logging disabled
{% endif %}
}
{% endif %}