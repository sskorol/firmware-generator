
// ===========================================================================
// =====                  Serial port API section                        =====
// ===========================================================================

char serialApiBuffer[{{ serial_api_buffer_size }}];
unsigned int serialApiBufferPos = 0;

void handleSerialService() {
  if (Serial.available() > 0) {
    while (Serial.available() > 0) {
      // buffer overflow protection
      if (serialApiBufferPos >= {{ serial_api_buffer_size }}) {
        Serial.println("serialApiBuffer overflow. Command size limit: {{ serial_api_buffer_size }} bytes. Ignore further data until CR char.");
        while (Serial.available() > 0) {
          char dataChar = Serial.read();
          if (dataChar == 10 || dataChar == 13) {
            serialApiBufferPos = 0; // reset buffer to be ready to start handling new command
            break;
          }
        }
      }
      serialApiBuffer[serialApiBufferPos++] = Serial.read();
      if (serialApiBuffer[serialApiBufferPos-1] == 10 || serialApiBuffer[serialApiBufferPos-1] == 13) { // detect end of command. Some Serial monitor tools use 10 character as end of input.
        serialApiBuffer[--serialApiBufferPos] = 0; // convert input to null terminated string
        if (serialApiBufferPos > 0) { // ignore empty input
          handleSerialInput(serialApiBuffer);
          serialApiBufferPos = 0; // reset buffer for next command
        }
      }
    }
  }
}

void handleSerialInput(const char* cmd) {
  Serial.print("handleSerialInput: ");
  Serial.println(cmd);
  if (strncmp(serialApiBuffer, "get", 3) == 0) {
    byte shift = serialApiBuffer[3] == 32 ? 4 : 3; // get parameters substring
    handleSerialGetRequest(&cmd[shift]);
  } else if (strncmp(serialApiBuffer, "set", 3) == 0) {
    byte shift = serialApiBuffer[3] == 32 ? 4 : 3; // get parameters substring
    handleSerialSetRequest(&cmd[shift]);
  } else if (strncmp(serialApiBuffer, "reboot", 5) == 0) {
    doReset();
  } else {
    Serial.print("unsupported command: ");
    Serial.println(cmd);
  }
}

void handleSerialGetRequest(const char* data) {
  Serial.print("handleSerialGetRequest: ");
  Serial.println(data);

  int dataReadPos = 0;
  while (data[dataReadPos] != 0) {
    // split data to param ids by the separator ','
    unsigned int dataStartPos = dataReadPos;
    while (data[dataReadPos++] != '=');
    char* errorMsg = 0;
    unsigned int paramId = parseParamIdStr(&data[dataStartPos], dataReadPos - dataStartPos, errorMsg);
    if (errorMsg != 0) {
      Serial.print("Unable to parse param ID: paramId=\"");
      Serial.println(&data[dataStartPos]);
      Serial.print("\", errorMsg=\"");
      Serial.print(errorMsg);
      Serial.println("\"");
      return;
    }
    // process parameter by ID
    paramValueToStr(paramId);
  }
  // return all parameters data if no param IDs was specified in request
  if (dataReadPos == 0) {
{% for param in params.values() %}
  Serial.print("{{ param.id }} ({{ param.num_id }}) = ");
  Serial.println(paramValueToStr({{ param.cid }}));
{% endfor %}
  }
}

void handleSerialSetRequest(const char* data) {
  Serial.print("handleSerialSetRequest: ");
  Serial.println(data);

  int dataReadPos = 0;
  while (data[dataReadPos] != 0) {
    // split data to param IDs and values by the separator ','
    // read param ID first
    unsigned int dataStartPos = dataReadPos;
    while (data[dataReadPos++] != '=');
    char* errorMsg = 0;
    unsigned int paramId = parseParamIdStr(&data[dataStartPos], dataReadPos - dataStartPos, errorMsg);
    if (errorMsg != 0) {
      Serial.print("Unable to parse param ID: paramId=\"");
      Serial.println(&data[dataStartPos]);
      Serial.print("\", errorMsg=\"");
      Serial.print(errorMsg);
      Serial.println("\"");
      return;
    }
    // read param value
    dataStartPos = dataReadPos;
    while (data[dataReadPos++] != ',' && data[dataReadPos] != 0);
    // process parameter by ID
    setParamValueFromSerial(paramId, &data[dataStartPos], dataReadPos - dataStartPos);
  }
}

void setParamValueFromSerial(unsigned int paramId, const char* paramValueStr, unsigned int paramValueStrLen) {
  Serial.print("setParamValueFromSerial: paramId=");
  Serial.print(paramId);
  Serial.print(", paramValueStr=\"");
  Serial.print(paramValueStr);
  Serial.print("\", paramValueStrLen=");
  Serial.println(paramValueStrLen);

  char* errorMsg = 0;
  void* paramValue = parseParamStrValue(paramId, paramValueStr, paramValueStrLen, errorMsg);
  if (errorMsg != 0) {
    Serial.print("Unable to parse param value: paramId=");
    Serial.print(paramId);
    Serial.print(", paramValueStr=\"");
    Serial.print(paramValueStr);
    Serial.print("\", errorMsg=\"");
    Serial.print(errorMsg);
    Serial.println("\"");
  } else {
    setParamValue(paramId, paramValue);
  }
}
