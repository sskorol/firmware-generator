byte nonsense_var = 0;  //this line fix compilation error in ArduinoIDE!

// ===========================================================================
// =====                      Includes section                           =====
// ===========================================================================
{% if use_eeprom %}
#include <EEPROM.h>  // flash memory to store items state
{% endif %}
{% if use_networking %}
#include <ESP8266WiFi.h>
#include <ESP8266WiFiType.h>
{% endif %}
{% if use_web_server %}
#include <ESP8266WebServer.h>
{% endif %}
{% if use_mdns %}
#include <ESP8266mDNS.h>
{% endif %}
{% if use_dht_lib %}
#include <dht.h>
{% endif %}
{% if use_one_wire_lib %}
#include <OneWire.h>
#include <DallasTemperature.h>
{% endif %}
{% if use_debounce_lib %}
#include <Bounce2.h>
{% endif %}

// ===========================================================================
// =====                      Defines section                            =====
// ===========================================================================
#define OFF 0
#define ON 1
#define OPEN 0
#define CLOSED 1
#define DISABLED 0
#define ENABLED 1

{% for status in list(Status) %}
#define STATUS_{{ status.name|upper }} {{ status.value}}
{% endfor %}
{% if use_networking %}
  {% for mode in wifi_modes_mapping.keys() %}
#define WIFI_MODE_{{ mode.name|upper }} {{ wifi_modes_mapping[mode] }}
  {% endfor %}

{% endif %}
{% if use_pwm_pins %}
#define PWM_MAX_VALUE {{ pwm_max_value }}
{% endif %}
{% if store_params_to_eeprom %}
#define EEPROM_START_ADDRESS {{ eeprom_start_address }}
{% endif %}
{% if use_debounce_lib %}
#define DEBOUNCER_INTERVAL {{ debounce_interval }}

{% endif %}
{% for pin in pins %}
#define {{ pin.cid }} {{ pin.pin_number | upper }}
{% endfor %}

{% for param in params_sorted_by_id %}
{# The '#-' + max_param_id_length + 's' means add space characters to the right #}
#define {{ ('%-' + max_param_cid_length + 's') | format(param.cid) | upper }} {{ param.num_id }}
{% endfor %}

// ===========================================================================
// =====                      Global variables                           =====
// ===========================================================================

{% if use_web_server %}
ESP8266WebServer webServer({{ http_server_port }});

{% endif %}
{% for param in params.values() %}
  {% if param.type.name in ['one_wire_address', 'ip_address'] %}
byte {{ param.cvar }}[{{ len(param.cvalue) }}] = {{'{' + ','.join(list(map(hex, param.cvalue))) + '}'}};
  {% else %}
{{ param.ctype }} {{ param.cvar }} = {{ param.cvalue }};
  {% endif %}
{% endfor %}

{% if use_debounce_lib %}
  {% for pin in contact_sensor_pins %}
Bounce {{ pin.debounce_var_name }} = Bounce();
  {% endfor %}

{% endif %}
{% if use_one_wire_lib %}
  {% for pin in one_wire_pins %}
OneWire {{ pin.one_wire_var_name }}_pin;
DallasTemperature {{ pin.one_wire_var_name }}(&{{ pin.one_wire_var_name }}_pin);
byte ONE_WIRE_EMPTY_ADDRESS[{{ len(ONE_WIRE_EMPTY_ADDRESS.value) }}] = {{'{' + ','.join(list(map(hex, ONE_WIRE_EMPTY_ADDRESS.value))) + '}'}};
  {% endfor %}

{% endif %}
byte paramValueBuffer[{{ param_value_buffer_size }}];

// declare method pointer for controller reset
void doReset () {
  ESP.reset();
}

// ===========================================================================
// =====                  setup and loop section                         =====
// ===========================================================================
{% if lock_heavy_operations %}
/*
 * This would be used to avoid an user input delay.
 * Heavy operations like DHT sensor read will set this flag to false. Idle loop will reset this flag to true.
 */
static bool allow_heavy_operations;
static unsigned long heavy_operations_lock_timestamp;
static unsigned long now;
{% endif %}

void loop()
{
{% if lock_heavy_operations %}
  now = millis();
  // process and release a heavy_operations_lock
  if (now < heavy_operations_lock_timestamp) {
    heavy_operations_lock_timestamp = 0; // in case of timer overflow
  }
  if ((unsigned long)(now - heavy_operations_lock_timestamp) > config_heavy_operations_lock_duration_state) {
    allow_heavy_operations = true;
  }

{% endif %}
{% if use_serial_api %}
  if (serial_api_enabled) {
    handleSerialService();
  }
{% endif %}
{% if use_web_server %}
  handleNetworkService();
{% endif %}
{% if use_contact_sensor_pins %}
  processContactSensors();
{% endif %}
{% if use_fade %}
  processFade();
{% endif %}
//  processDigitalSensors();
//  processPowerControl();
{% if use_networking %}
{% endif %}
  delay(10);
}

void setup()
{
{% if use_serial %}
  Serial.begin({{ serial_port_bodrate }});

{% endif %}
{% if use_eeprom %}
  logln("Reading data from EEPROM... ");
  readEEPROMData();
  blinkStatusLed();
  logln("Done.");

{% endif %}
{% if use_networking %}
  logln("Configuring wifi... ");
  setupWifi();
  logln("Done.");
  {% if use_web_server %}

  logln("Configuring web server... ");
  initWebServer();
  logln("Done.");
  {% endif %}

{% endif %}
{% if len(contact_sensor_pins) > 0 %}
  // setup contact sensors like door sensors or light switches
  logln("Configuring contact sensor PINs... ");
  {% for pin in contact_sensor_pins %}
  pinMode({{ pin.cid }}, INPUT_PULLUP);
    {% if use_debounce_lib %}
  {{ pin.debounce_var_name }}.attach({{ pin.cid }});
  {{ pin.debounce_var_name }}.interval(DEBOUNCER_INTERVAL);
    {% endif %}
  {% endfor %}
  logln("Done.");

  // check the real state of contact sensors
  logln("Reading contact sensors initial state... ");
  processInitialContactSensorsState();
  logln("Done.");

{% endif %}
{% if len(dout_pins) > 0 %}
  // setup GPIO digital out pins for RELAY switches
  logln("Configuring GPIO out pins... ");
{% for pin in dout_pins %}
  pinMode({{ pin.cid }}, OUTPUT);
{% endfor %}
  logln("Done.");

{% endif %}
{% if use_one_wire_lib %}
  logln("Configuring One Wire devices... ");
  {% for pin in one_wire_pins %}
  {{ pin.one_wire_var_name }}.begin();
    {% for device in one_wire_sensors if device.pin == pin %}
  if (!isEqualAddresses({{ device.address_param.cvar }}, ONE_WIRE_EMPTY_ADDRESS)) {
    {{ pin.one_wire_var_name }}.setResolution({{ device.address_param.cvar }}, {{ device.precision }});
    log("{{ device.id }} one wire address: ");
    logOneWireAddress({{ device.address_param.cvar }});
  }
    {% endfor %}
  {% endfor %}
  logln("Done.");

{% endif %}
{% if use_relay_devices %}
  logln("Apply initial state of relay devices... ");
  {% for device in relay_devices %}
  applyRelayItemState({{ device.switch_param.cid }});
  {% endfor %}
  logln("Done.");

{% endif %}
{% if use_light_devices %}
  {% for device in relay_devices %}
  logln("Apply initial state of light devices... ");
  applyLightItemState({{ device.switch_param.cid }});
  {% endfor %}
  logln("Done.");

{% endif %}
  logln("Setup complete.");
}

{% if use_networking %}
void setupWifi() {
  WiFi.hostname("{{ ip_host_name }}");
  if (wifi_mode == WIFI_MODE_ACCESS_POINT) {
    IPAddress ip(ip_address);
    IPAddress mask(ip_mask);
    IPAddress gateway(ip_gateway);
    log("Setting soft-AP configuration ... ");
    logln(WiFi.softAPConfig(ip, gateway, mask) ? "Ready" : "Failed!");
    log("Setting soft-AP ... ");
    logln(WiFi.softAP(wifi_ap_ssid.c_str(), wifi_ap_password.c_str()) ? "Ready" : "Failed!");
    log("Soft-AP IP address = ");
    logln(WiFi.softAPIP());
    Serial.printf("SSID: %s\n", WiFi.SSID().c_str());
    Serial.printf("PSK: %s\n", WiFi.psk().c_str());
  } else if (wifi_mode == WIFI_MODE_STATIC_CLIENT) {
    IPAddress ip(ip_address);
    IPAddress mask(ip_mask);
    IPAddress gateway(ip_gateway);
    WiFi.config(ip, mask, gateway);
    WiFi.begin(wifi_ap_ssid.c_str(), wifi_ap_password.c_str());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
  } else if (wifi_mode == WIFI_MODE_DHCP_CLIENT) {
    WiFi.begin(wifi_ap_ssid.c_str(), wifi_ap_password.c_str());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
  {% if "ip_address" in params.keys() %}
    setParamValue({{ params["ip_address"].cid }}, &(WiFi.localIP())[0]);
  {% endif %}
  {% if "ip_address" in params.keys() %}
    setParamValue({{ params["ip_mask"].cid }}, &(WiFi.subnetMask())[0]);
  {% endif %}
  {% if "ip_address" in params.keys() %}
    setParamValue({{ params["ip_gateway"].cid }}, &(WiFi.gatewayIP())[0]);
  {% endif %}
  } else {
    log("unsupported wifi mode");
    return;
  }

{% if use_mdns %}
  if ( MDNS.begin ("{{mdns_name}}") ) {
    logln( "MDNS responder started" );
  }
{% endif %}
  delay(500);
  WiFi.printDiag(Serial);
  log("Mode: ");
  logln(wifi_mode);
  log("Connected to: ");
  logln(wifi_ap_ssid);
  log("IP address: ");
  logln(WiFi.localIP());
}

{% endif %}
{% if len(contact_sensor_pins) > 0 %}
void processInitialContactSensorsState()
{
  {% for sensor in contact_sensors %}
    {% if use_debounce_lib %}
  {{ sensor.pin.debounce_var_name }}.update();
  {{ sensor.pin.debounce_var_name }}.interval(DEBOUNCER_INTERVAL);
  {{ sensor.state_param.cvar }} = ({{ sensor.pin.debounce_var_name }}.read() == HIGH ? OFF : ON);
    {% else %}
  {{ sensor.state_param.cvar }} = (digitalRead({{ sensor.pin.cid }}) == HIGH ? OFF : ON);
    {% endif %}
  {% endfor %}
}

{% endif %}
{% include "nodemcu_eeprom_part.ino" %}
{% if use_web_server %}
{% include "nodemcu_http_api_part.ino" %}
{% endif %}
{% if use_serial_api %}
{% include "nodemcu_serial_api_part.ino" %}
{% endif %}

// ===========================================================================
// =====                     Control section                             =====
// ===========================================================================

{% if use_relay_devices %}
void applyRelayItemState(int itemId) {
  int value=-1;
  switch (itemId) {
  {% for device in relay_devices %}
    case {{ device.switch_param.cid }}:
      value = {{ device.switch_param.cvar }};
      digitalWrite({{ device.pin.cid }}, value == OFF ? {% if device.pin.inverted %} HIGH : LOW {% else %} LOW : HIGH {% endif %});
      break;
  {% endfor %}
    default:
      log("appplyRelayItemState Unknown itemId.");
      break;
  }
  log("appplyRelayItemState itemId=");
  log(itemId);
  log(", value=");
  logln(value);
}

{% endif %}
{% if len(contact_sensors) > 0 %}
void processContactSensors()
{
  byte newState; // lets assume that contact sensor state is always byte type
  {% for device in wall_switch_devices %}
  {{ device.pin.debounce_var_name }}.update();
  newState = ({{ device.pin.debounce_var_name }}.read() == HIGH ? {% if device.pin.inverted %}ON : OFF{% else %}OFF : ON{% endif %});
  if (newState != {{ device.state_param.cvar }}) {
    setParamValue({{ device.state_param.cid }}, &newState);
  }
  {% endfor %}
  {% for device in reed_switch_sensors %}
  {{ device.pin.debounce_var_name }}.update();
  newState = ({{ device.pin.debounce_var_name }}.read() == HIGH ? {% if device.pin.inverted != device.normally_opened %}OPEN : CLOSED{% else %}CLOSED : OPEN{% endif %});
  if (newState != {{ device.state_param.cvar }}) {
    setParamValue({{ device.state_param.cid }}, &newState);
  }
  {% endfor %}
}

{% endif %}
void setParamValue(unsigned int paramId, void* paramValue) {
  log("setParamValue:  paramId=");
  Serial.print(paramId);
  Serial.print(", paramValue=");
  Serial.println(*(byte *) paramValue);
  switch (paramId){
{% for param in params.values() %}
    case {{ param.cid }}:
  {% if param.ctype == 'byte' %}
      {{ param.cvar }} = *(byte *) paramValue;
  {% elif  param.ctype == 'word' %}
      {{ param.cvar }} = *(word *) paramValue;
  {% elif  param.ctype == 'int' %}
      {{ param.cvar }} = *(int *) paramValue;
  {% elif  param.ctype == 'float' %}
      {{ param.cvar }} = *(float *) paramValue;
  {% elif  param.ctype == 'IPAddress' %}
      {{ param.cvar }}[0] = ((byte *) paramValue)[0];
      {{ param.cvar }}[1] = ((byte *) paramValue)[1];
      {{ param.cvar }}[2] = ((byte *) paramValue)[2];
      {{ param.cvar }}[3] = ((byte *) paramValue)[3];
  {% endif %}
  {% if isinstance(param.device, Relay) %}
      applyRelayItemState({{ param.cid }});
  {% endif %}
      break;
{% endfor %}
    default:
      Serial.print("Unknown param ID: ");
      Serial.println(paramId);
      break;
  }
  onParamStateChange(paramId, paramValue);
}

void onParamStateChange(int paramId, void* newValue) {
  {% if isinstance(rules_engine, PredefinedRulesEngine) %}
  switch (paramId){
    {% for param in params_sorted_by_id %}
      {% if len(rules_engine.get_rules_by_trigger_param(paramId)) > 0 %}
    case {{ param.cid }}:
        {% for rule in rules_engine.get_rules_by_trigger_param(paramId) %}
          {% if isinstance(rule, TranslateParamValueRule) %}
            {% if is_simple_param_type(param.type) %}
      {{ rule.target_param.cvar }} = {{ rule.source_param.cvar }};
            {% else %}
      copy_variable()
            {% endif %}
          {% endif %}
        {% endfor %}
      {% endif %}
    {% endfor %}

    {% for rule in rules_engine.rules %}
      {% if isinstance(rule, TranslateParamValueRule) %}
    case
  {{ rule.target_param.cvar }} = {{ rule.source_param.cvar }};
      {% endif %}
    {% endfor %}
  }
  {% endif %}
}

unsigned int parseParamIdStr(const char* paramIdStr, byte paramIdStrLen,  const char* error) {
  unsigned int paramId = 0;
  for (byte dataReadPos = 0; dataReadPos < paramIdStrLen; dataReadPos++) {
    int dataChar = paramIdStr[dataReadPos++];
    if (dataChar < '0' || dataChar > '9') {
      error = "unsupported character in param ID";
      return paramId;
    }
    if (paramId > {{ unsigned_int_max_value }} / 10) {
      error = "param ID exceeds the UINT_MAX limit";
      return paramId;
    }
    paramId = paramId * 10 + dataChar - '0';
  }
  return paramId;
}

void* parseParamStrValue(unsigned int paramId, const char* paramValueStr, unsigned int paramValueStrLen, const char* error) {
  unsigned int dataReadPos = 0;
  switch (paramId){
{% for param in params.values() %}
  {% if param.ctype == 'byte' %}
    case {{ param.cid }}:
  {% endif %}
{% endfor %}
      {
        byte* value = paramValueBuffer;
        *value = byte(0);
        while (dataReadPos < paramValueStrLen) {
            int dataChar = paramValueStr[dataReadPos++];
            if (dataChar < '0' || dataChar > '9') {
              error = "wrong param value format";
              return value;
            }
            if (paramId > 0xff / 10) {
              error = "param ID exceeds the BYTE_MAX limit";
              return value;
            }
            *value = *value * 10 + dataChar - '0';
          }
      }
      break;
    default:
      error = "not supported param type";
      break;
  }
  return paramValueBuffer;
}

String paramValueToStr(unsigned int paramId) {
  switch (paramId) {
{% for param in params.values() %}
    case {{ param.cid }}:
  {% if param.type.name == 'string' %}
      return {{ param.cvar }};
  {% elif is_simple_param_type(param.type) %}
      return String({{ param.cvar }});
  {% elif param.type.name == 'one_wire_address' %}
      return oneWireAddressToStr({{ param.cvar }});
  {% elif param.type.name == 'ip_address' %}
      return ipAddressToStr({{ param.cvar }});
  {% else %}
      return "< not implemented yet >";
  {% endif %}
{% endfor %}
    default:
      log("Unknown param ID");
      log(int(paramId));
      return "< unknown param ID >";
  }
}

char* oneWireAddressToStr(byte* value) {
  for (byte i = 0; i < 8; i++) {
    byte octet = value[i] >> 8;
    paramValueBuffer[i] = octet > 9 ? 'A' + octet - 9 : '0' + octet;
    octet = value[i] & 0xf;
    paramValueBuffer[i+1] = octet > 9 ? 'A' + octet - 9 : '0' + octet;
  }
  paramValueBuffer[16] = 0;
  return (char*) paramValueBuffer;
}

char* ipAddressToStr(byte* value) {
  byte strWritePos = 0;
  for (byte i = 0; i < 4; i++) {
    if (strWritePos > 0) {
      paramValueBuffer[strWritePos++] = '.';
    }
    byte digit = value[i] / 100;
    if (digit > 0) {
      paramValueBuffer[strWritePos++] = '0' + digit;
    }
    digit = (value[i] - digit * 100) / 10;
    if (digit > 0) {
      paramValueBuffer[strWritePos++] = '0' + digit;
    }
    digit = value[i] % 10;
    paramValueBuffer[strWritePos++] = '0' + digit;
  }
  paramValueBuffer[strWritePos++] = 0;
  return (char*) paramValueBuffer;
}

{% include "nodemcu_logging_part.ino" %}
{% include "nodemcu_utility_part.ino" %}