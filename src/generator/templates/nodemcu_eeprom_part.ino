{% if use_eeprom %}
// ===========================================================================
// =====                       EEPROM section                            =====
// ===========================================================================

bool factory_reset_flag = {{ 'true' if do_reset_eeprom_on_startup else 'false' }};

void doFactoryReset() {
  factory_reset_flag = true;
  updateEEPROMData();
  doReset();
}

void updateEEPROMData()
{
  int address = EEPROM_START_ADDRESS;
  EEPROM.update(address++, factory_reset_flag);
{% for param in eeprom_stored_params %}
  {% if param.type.name == 'one_wire_address' %}
    {% for i in range(8) %}
  EEPROM.update(address++, {{ param.id }}[{{ i }}]);
    {% endfor %}
  {% elif param.type.name in ['ip_address', 'long', 'float'] %}
    {% for i in range(4) %}
  EEPROM.update(address++, ((byte*)(&{{ param.id }}))[{{ i }}]);
    {% endfor %}
  {% elif param.type.name == 'string' %}
  for (int i=0; {{ param.id }}[i] != 0; i++) {
    EEPROM.update(address++, ((byte*)(&{{ param.id }}))[i]);
  }
  EEPROM.update(address++, 0); // end of zero terminated string variable {{ param.id }}
  {% else %}
  EEPROM.update(address++, {{ param.id }});
  {% endif %}
{% endfor %}
}

void readEEPROMData()
{
  int address = EEPROM_START_ADDRESS;
  bool is_factory_reset = EEPROM.read(address++);
  if (is_factory_reset) {
    //if factory reset action was performed then do not read EEPROM data but override EEPROM with default values instead.
    updateEEPROMData();
    logln("-= factory reset performed =-");
    return;
  }

{% for param in eeprom_stored_params %}
  {% if param.type.name == 'one_wire_address' %}
    {% for i in range(8) %}
  {{ param.id }}[{{ i }}] = EEPROM.read(address++);
    {% endfor %}
  {% elif param.type.name in ['ip_address', 'long', 'float'] %}
    {% for i in range(4) %}
  ((byte*)(&{{ param.id }}))[{{ i }}] = EEPROM.read(address++);
    {% endfor %}
  {% elif param.type.name == 'string' %}
  for (int i=0; {{ param.id }}[i] != 0; i++) {
    EEPROM.update(address++, ((byte*)(&{{ param.id }}))[i]);
  }
  {% else %}
  {{ param.id }} = EEPROM.read(address++);
  {% endif %}
{% endfor %}
}
{% endif %}