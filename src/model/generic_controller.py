from enum import Enum


class Pin(object):
    def __init__(self, id: str=None, pin_number: int=None, type: str=None):
        self.pin_number = pin_number.value if isinstance(pin_number, Enum) else pin_number
        self.id = id
        self.type = type

    def __eq__(self, other):
        return self.id == other.id


class PwmPin(Pin):
    def __init__(self, id: str=None, pin_number: int=None, inverted: bool=False):
        super(PwmPin, self).__init__(id=id, pin_number=pin_number, type='pwm')
        self.inverted = inverted


class ContactSensorPin(Pin):
    def __init__(self, id: str=None, pin_number: int=None, inverted: bool=False):
        super(ContactSensorPin, self).__init__(id=id, pin_number=pin_number, type='contact')
        self.inverted = inverted


class DOutPin(Pin):
    def __init__(self, id: str=None, pin_number: int=None, inverted: bool=False):
        super(DOutPin, self).__init__(id=id, pin_number=pin_number, type='dout')
        self.inverted = inverted


class OneWirePin(Pin):
    def __init__(self, id: str=None, pin_number: int=None):
        super(OneWirePin, self).__init__(id=id, pin_number=pin_number, type='one_wire')


class DhtPin(Pin):
    def __init__(self, id: str=None, pin_number: int=None):
        super(DhtPin, self).__init__(id=id, pin_number=pin_number, type='dht')
