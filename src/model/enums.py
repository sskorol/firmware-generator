from enum import Enum


class Status(Enum):
    unknown = 0
    ok = 1
    error = 2
    disabled = 3


class SwitchState(Enum):
    off = 0
    on = 1


class OpenState(Enum):
    open = 0
    closed = 1


class WifiMode(Enum):
    disabled = 0
    access_point = 1
    dhcp_client = 2
    static_client = 3
