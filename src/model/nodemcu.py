from functools import partial
from enum import Enum

from model.ip_address import IpAddress
from model.parameter import ParameterType, Parameter
from model.device import AbstractDevice
from model.one_wire import DS18B20Sensor
from model.dht import DhtSensor
from model.contact_sensor import ContactSensor, WallSwitch, ReedSwitchSensor
from model.light import LightSource
from model.relay import Relay
from model.rules_engine import RulesEngine, PredefinedRulesEngine
from model.web_server import WebServer
from model.generic_controller import PwmPin, ContactSensorPin, OneWirePin, DOutPin, DhtPin
from model.enums import WifiMode


class Pinout(Enum):
    D0 = 16
    D1 = PWM1 = 5
    D2 = PWM2 = 4
    D3 = PWM3 = 0
    D4 = PWM4 = TX1 = 2
    D5 = PWM5 = 14
    D6 = PWM6 = 12
    D7 = PWM7 = RX2 = 13
    D8 = PWM8 = TX2 = 15
    D9 = RX0 = 3
    D10 = TX0 = 1


class NodeMcu(AbstractDevice):
    def __init__(self, id=None, name=None):
        super(NodeMcu, self).__init__(id=id, name=name)

        self.wifi_mode = WifiMode.access_point
        self.wifi_ap_ssid = 'nodemcu_ap'
        self.wifi_ap_password = '12345678'

        self.ip_address = IpAddress('192.168.1.1')
        self.ip_mask = IpAddress('255.255.255.0')
        self.ip_gateway = IpAddress('192.168.1.1')
        self.ip_host_name = "nodemcu-board"
        self.http_server_port = 80

        self.networking_feature_enabled = True
        self.serial_port_bodrate = 115200
        self.serial_api_feature_enabled = True

        self.mdns_feature_enabled = False
        self.mdns_name = self.ip_host_name

        self.log_server_ip_address = IpAddress('192.168.1.2')
        self.log_server_port = 8010
        self.http_client = "simple_http_client"

        self.serial_logging_feature_enabled = False  # send debug info to serial port
        self.network_logging_feature_enabled = False  # send debug info over network to the log_server  t TCP socket
        self.web_server_feature_enabled = True
        self.debounce_feature_enabled = True
        self.eeprom_feature_enabled = True
        self.lock_heavy_operations = True
        self.allow_reboot = True  # network option
        self.allow_firmware_reset = True  # network option
        self.do_reset_eeprom_on_startup = True  # development option. Should be set to false before release version.

        self.debounce_interval = 5
        self.one_wire_sensor_read_interval = 10000  # how often the one_wire bus will be requested for data
        self.log_server_ping_period_state = 30000   # in case if any log server error (usually it is a connection error)
                                                    # it will be disabled for this period (in milliseconds) and then
                                                    # the log server will be checked each N milliseconds until the log
                                                    # server response will become successful
        self.eeprom_start_address = 0
        self.eeprom_stored_params = []  # these params are initialized in init_params method
        self.unsigned_int_max_value = 0xffffffff
        self.pwm_max_value = 1023
        self.url_buf_size = 100
        self.serial_api_buffer_size = 100
        self.param_value_buffer_size = 100
        # self.get_items_buf_size = 100
        # self.post_items_buf_size = 100
        self.post_address_items_buf_size_limit = None
        self.network_logging_enabled = True

        self.devices = []
        self.pins = []

        self.web_server = WebServer()
        self.rules_engine = PredefinedRulesEngine()

    def init_params(self, global_params_holder):
        super(NodeMcu, self).init_params(global_params_holder)
        self.eeprom_stored_params.clear()
        add_param = partial(self.add_param, global_params_holder=global_params_holder)
        add_eeprom_param = partial(self.add_eeprom_param, global_params_holder=global_params_holder)

        # init nodemcu own parameters
        if self.use_one_wire_lib():
            add_eeprom_param(Parameter(type=ParameterType.long, id='one_wire_sensor_read_interval', value=self.one_wire_sensor_read_interval))
        if self.use_network_logging():
            add_eeprom_param(Parameter(type=ParameterType.boolean, id='network_logging_enabled', value=self.network_logging_enabled))
            add_param(Parameter(type=ParameterType.status, id='status_log_server_connection'))
            add_eeprom_param(Parameter(type=ParameterType.ip_address, id='log_server_ip_address', value=self.log_server_ip_address))
            add_eeprom_param(Parameter(type=ParameterType.int, id='log_server_port', value=self.log_server_port))
        if self.use_networking():
            add_eeprom_param(Parameter(type=ParameterType.wifi_mode, id='wifi_mode', value=self.wifi_mode))
            add_eeprom_param(Parameter(type=ParameterType.string, id='wifi_ap_ssid', value=self.wifi_ap_ssid))
            add_eeprom_param(Parameter(type=ParameterType.string, id='wifi_ap_password', value=self.wifi_ap_password))
            add_eeprom_param(Parameter(type=ParameterType.ip_address, id='ip_address', value=self.ip_address))
            add_eeprom_param(Parameter(type=ParameterType.ip_address, id='ip_mask', value=self.ip_mask))
            add_eeprom_param(Parameter(type=ParameterType.ip_address, id='ip_gateway', value=self.ip_gateway))
            add_eeprom_param(Parameter(type=ParameterType.int, id='http_server_port', value=self.http_server_port))
        if self.use_serial():
            add_param(Parameter(type=ParameterType.boolean, id='serial_api_enabled', value=self.use_serial_api, read_only=True))

        # init eeprom_stored_params for the owned devices. All devices params should be initialized first.
        for device in self.devices:
            if isinstance(device, DS18B20Sensor):
                self.eeprom_stored_params.extend(p for p in device.params.values() if p.id.endswith('_address'))
            if isinstance(device, LightSource):
                self.eeprom_stored_params.extend(device.params.values())

    def add_eeprom_param(self, param, global_params_holder=None):
        """ Add parameter both to self.params list and to self.eeprom_stored_params list. """
        self.add_param(param, global_params_holder)
        self.eeprom_stored_params.append(param)

    def use_logging(self):
        return self.use_serial_logging() or self.use_network_logging()

    def use_serial_logging(self):
        return self.serial_logging_feature_enabled

    def use_serial_api(self):
        return self.serial_api_feature_enabled

    def use_serial(self):
        return self.use_serial_logging() or self.use_serial_api()

    def use_one_wire_lib(self):
        for device in self.devices:
            if isinstance(device, DS18B20Sensor):
                return True
        return False

    def use_dht_lib(self):
        for device in self.devices:
            if isinstance(device, DhtSensor):
                return True
        return False

    def use_debounce_lib(self):
        return self.debounce_feature_enabled and len(self.contact_sensor_pins()) > 0

    def use_networking(self):
        return self.networking_feature_enabled

    def use_network_logging(self):
        return self.use_networking() and self.network_logging_feature_enabled

    def use_web_server(self):
        return self.use_networking() and self.web_server_feature_enabled

    def use_mdns(self):
        return self.use_networking() and self.mdns_feature_enabled

    def use_relay_devices(self):
        return len(self.relay_devices()) > 0

    def use_light_devices(self):
        return len(self.light_devices()) > 0

    def use_contact_sensors(self):
        return len(self.contact_sensors()) > 0

    def use_pwm_pins(self):
        return len(self.pwm_pins()) > 0

    def use_contact_sensor_pins(self):
        return len(self.contact_sensor_pins()) > 0

    def use_fade(self):
        return self.use_light_devices() and any(light.use_fade_on_switch for light in self.light_devices())

    def pwm_pins(self):
        return [pin for pin in self.pins if isinstance(pin, PwmPin)]

    def contact_sensor_pins(self):
        return [pin for pin in self.pins if isinstance(pin, ContactSensorPin)]

    def dout_pins(self):
        return [pin for pin in self.pins if isinstance(pin, DOutPin)]

    def one_wire_pins(self):
        return [pin for pin in self.pins if isinstance(pin, OneWirePin)]

    def dht_pins(self):
        return [pin for pin in self.pins if isinstance(pin, DhtPin)]

    def wall_switch_devices(self):
        return [device for device in self.devices if isinstance(device, WallSwitch)]

    def reed_switch_sensors(self):
        return [device for device in self.devices if isinstance(device, ReedSwitchSensor)]

    def contact_sensors(self):
        return [device for device in self.devices if isinstance(device, ContactSensor)]

    def one_wire_sensors(self):
        return [device for device in self.devices if isinstance(device, DS18B20Sensor)]

    def relay_devices(self):
        return [device for device in self.devices if isinstance(device, Relay)]

    def light_devices(self):
        return [device for device in self.devices if isinstance(device, LightSource)]

    def post_address_items_buf_size(self):
        """
        How many onewire device addresses you could setup by a single POST request.
        """
        result = 0
        if self.post_address_items_buf_size_limit is None:
            for device in self.devices:
                if isinstance(device, DS18B20Sensor):
                    result += 1
        else:
            result = self.post_address_items_buf_size_limit
        return result
