# This is a representation of single valuable parameter of any real or virtual device.
# Each parameter has it's own ID, value and some additional attributes like name alias or is_writable.
# Parameter ID is a main
from numbers import Number
from enum import Enum

from model.ip_address import IpAddress
from model.one_wire_address import OneWireAddress
from model.enums import Status, SwitchState, OpenState, WifiMode


class ParameterType(Enum):
    boolean = 0
    byte = 1
    word = 2
    int = 3
    long = 4
    float = 5
    string = 6
    ip_address = 7
    one_wire_address = 8
    status = 9
    switch_state = 10
    open_state = 11
    wifi_mode = 12

    @staticmethod
    def get_default_value(parameter_type):
        if parameter_type == ParameterType.boolean:
            return 0
        elif parameter_type == ParameterType.byte:
            return 0
        elif parameter_type == ParameterType.word:
            return 0
        elif parameter_type == ParameterType.int:
            return 0
        elif parameter_type == ParameterType.long:
            return 0
        elif parameter_type == ParameterType.float:
            return 0.0
        elif parameter_type == ParameterType.string:
            return None
        elif parameter_type == ParameterType.status:
            return Status.unknown
        elif parameter_type == ParameterType.switch_state:
            return SwitchState.off
        elif parameter_type == ParameterType.open_state:
            return OpenState.open
        elif parameter_type == ParameterType.ip_address:
            return IpAddress()
        elif parameter_type == ParameterType.one_wire_address:
            return OneWireAddress()
        elif parameter_type == ParameterType.wifi_mode:
            return WifiMode.disabled


class Parameter:
    def __init__(self, num_id=None, id=None, type=ParameterType.byte, value=None, read_only=True, device=None, device_value_path=None):
        self.num_id = num_id
        self.id = id
        self.type = type
        self.value = value if value is not None else ParameterType.get_default_value(type)
        self.read_only = read_only
        self.device = device
        self.device_value_path = device_value_path

    def is_int(self):
        if self.type in [ParameterType.boolean, ParameterType.byte, ParameterType.long]:
            return True
        return False

    def is_read_only(self):
        return self.read_only

    def is_writable(self):
        return not self.read_only

    def read_value(self):
        if self.device_value_path is not None:
            return self.device[self.device_value_path]  # TODO: improve this mechanism
        else:
            return self.value


class ParameterHolder:
    def __init__(self):
        self.params_by_num_id_dict = {}
        self.last_arranged_num_id = 0

    def __iter__(self):
        return self.params_by_num_id_dict.__iter__()

    def add(self, param):
        if param.num_id is not None:
            if param.id in self.params_by_num_id_dict and param is not self.params_by_num_id_dict[param.id]:
                raise ValueError('Parameter with the same numerical ID is already registered in ParameterHolder. Param num_id = {}'
                                 .format(param.num_id))
        else:
            while self.last_arranged_num_id in self.params_by_num_id_dict:
                self.last_arranged_num_id += 1
            param.num_id = self.last_arranged_num_id

        self.params_by_num_id_dict[param.num_id] = param
        return param

    def get(self, num_id_or_id):
        """ Return Parameter object based on its num_id value or on id value. """
        if isinstance(num_id_or_id, Number):
            return self.parameters_dict[num_id_or_id]
        else:
            for param in self.parameters_dict:
                if param.id == num_id_or_id:
                    return param
        return None
