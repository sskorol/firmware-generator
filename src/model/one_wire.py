from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType
from model.one_wire_address import ONE_WIRE_EMPTY_ADDRESS


class DS18B20Sensor(AbstractDevice):
    """
    This is a representation of Dallas DS18B20 temperature sensor. See more details here: http://arduino-project.net/podklyuchenie-ds18b20-arduino/
    """
    def __init__(self, pin, id=None, name=None, one_wire_address=ONE_WIRE_EMPTY_ADDRESS, read_interval=10000):
        """
        :param pin: OneWirePin which DS18B20Sensor is connected.
        :param id: device ID string value only lower case latin letters and underscore sign are allowed.
        :param name: display name of device as Unicode str.
        :param one_wire_address: OneWireAddress object.
        :param read_interval: how often this sensor value will be updated. In milliseconds.
        """
        super(DS18B20Sensor, self).__init__(id=id, name=name)
        self.pin = pin
        self.one_wire_address = one_wire_address
        self.read_interval = read_interval
        self.precision = 11  # 8, 9, 10 or 11 bit length value
        self.current_temperature = 0
        self.status_param = None
        self.temperature_param = None
        self.address_param = None

    def init_params(self, global_params_holder):
        super(DS18B20Sensor, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        self.status_param = add_param(Parameter(type=ParameterType.status, id='one_wire_{}_status'.format(self.id)))
        self.temperature_param = add_param(Parameter(type=ParameterType.float, id='one_wire_{}_temperature'.format(self.id)))
        self.address_param = add_param(Parameter(type=ParameterType.one_wire_address, id='one_wire_{}_address'.format(self.id)))
