from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType
from enum import Enum


class SwitchState(Enum):
    OFF = 0
    ON = 1


class LightChannel:
    def __init__(self, id=None, name=None, min_value=0, max_value=255, value=255):
        self.id = id
        self.name = name
        self.min_value = min_value
        self.max_value = max_value
        self.value = value


class PwmLightChannel(LightChannel):
    def __init__(self, pin, id=None, name=None, min_value=0, max_value=255, value=255):
        super(PwmLightChannel, self).__init__(id=id, name=name, min_value=min_value, max_value=max_value, value=value)
        self.pin = pin


class LightSource(AbstractDevice):
    """ Light source. Could be dimmable with multiple channels. """
    def __init__(self, id=None, name=None, dimmable=False, channels=None, use_fade_on_switch=False):
        super(LightSource, self).__init__(id=id, name=name)
        self.dimmable = dimmable
        self.channels = channels  # list of LightChannel objects
        self.use_fade_on_switch = use_fade_on_switch
        self.switch_state = SwitchState.OFF
        self.dimmer_max_value = 100
        self.dimmer_min_value = 1  # This is different from 0 in order to differentiate a 0 brightness from a switch_state = OFF.
        self.dimmer_state = self.dimmer_max_value
        self.fade_interval = 30  # Time period in milliseconds between two calls of fade value update. Smaller value means faster and smoother fade effect.
        self.fade_increment = 5  # How much the fade_state value will decreased per single fade step. Bigger value means faster fade effect.
        self.fade_state = 0  # That means that no fade effect is in progress right now.
        self.switch_param = None
        self.dimmer_param = None
        self.channel_params = []

    def init_params(self, global_params_holder):
        super(LightSource, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        self.switch_param = add_param(Parameter(type=ParameterType.boolean, id='light_{}_switch'.format(self.id)))
        self.dimmer_param = add_param(Parameter(type=ParameterType.byte, id='light_{}_dimmer'.format(self.id)))
        if self.channels is not None:
            for channel_index, channel in enumerate(self.channels):
                self.channel_params.append(add_param(
                    Parameter(type=ParameterType.byte,
                              id='light_{}_channel_{}'.format(self.id, channel.id or channel_index))))


class PwmBasedLightSource(LightSource):
    """ Light source which is directly connected to PwmPins. Could be dimmable with multiple channels. """
    def __init__(self, id=None, name=None, dimmable=False, channels=None, use_fade_on_switch=False):
        super(PwmBasedLightSource, self).__init__(id=id, name=name, dimmable=dimmable, channels=channels, use_fade_on_switch=use_fade_on_switch)
