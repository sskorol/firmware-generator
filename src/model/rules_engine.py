

class RulesEngine:
    def __init__(self, rules=None):
        self.rules = rules or []

    def on_param_changed_rules(self):
        return [rule for rule in self.rules if isinstance(rule, OnParamChangedRule)]


class PredefinedRulesEngine(RulesEngine):
    def get_rules_by_trigger_param(self, param):
        result = []
        result.extend([rule for rule in self.rules
                       if ((rule, TranslateParamValueRule) and param == rule.source_param) or
                          ((rule, SendParamToUrlRule) and param in rule.params)])
        return result


class OnParamChangedRule:
    """ This rule is triggering when some parameter value was changed. """
    pass


class OnStartupRule:
    """ This rule is triggering just after the startup initialization finished. """
    pass


class TranslateParamValueRule(OnParamChangedRule):
    """ This rule applies the value of source param to the target param. """
    def __init__(self, source_param, target_param):
        self.source_param = source_param
        self.target_param = target_param


class TranslateParamValueOnStartupRule(OnStartupRule):
    """ This rule applies the value of source param to the target param. """
    def __init__(self, source_param, target_param):
        self.source_param = source_param
        self.target_param = target_param


class SendParamToUrlRule(OnParamChangedRule):
    """ This rule checks if param is mentioned in params list and send "url + param.cid + param.value". """
    def __init__(self, params, url):
        self.params = params
        self.url = url

