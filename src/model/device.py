from collections import OrderedDict
from functools import partial


class AbstractDevice(object):
    def __init__(self, id=None, name=None, status=None):
        self.id = id
        self.name = name
        self.status = status
        self.params = OrderedDict()

    def init_params(self, global_params_holder):
        """ Register all device parameters in global_params_holder. """
        self.params.clear()

    def add_param(self, param, global_params_holder=None):
        """ Add new parameter to self.params dictionary (use param.name as dict key) and register it in global_params_holder """

        self.params[param.id] = param
        if param.device is None:
            param.device = self
        if global_params_holder is not None:
            global_params_holder.add(param)
        return param

    def has_status(self):
        """ Return true if this device is able to provide its status. """
        return self.status is not None
