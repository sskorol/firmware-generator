from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType
from model.enums import Status


class WebServer(AbstractDevice):
    """ Web server software representation. """
    def __init__(self, id=None, name=None):
        super(WebServer, self).__init__(id=id, name=name, status=Status.unknown)

    def init_params(self, global_params_holder):
        super(WebServer, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        add_param(Parameter(type=ParameterType.status, id='status'))
