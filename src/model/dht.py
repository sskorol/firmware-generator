from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType


class DhtSensor(AbstractDevice):
    """ This is a representation of DHT temperature and Humidity sensor.
        See more details here: http://arduino-project.net/podklyuchenie-datchika-dht11-arduino/"""
    def __init__(self, id=None, name=None):
        super(DhtSensor, self).__init__(id=id, name=name)
        self.current_temperature = 0.0
        self.current_humidity = 0.0

    def init_params(self, global_params_holder):
        super(DhtSensor, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        add_param(Parameter(type=ParameterType.status, id='status_dht_{}_state'.format(self.id)))
        add_param(Parameter(type=ParameterType.float, id='temperature_dht_{}_state'.format(self.id)))
        add_param(Parameter(type=ParameterType.float, id='humidity_dht_{}_state'.format(self.id)))
