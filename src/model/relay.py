from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType


class Relay(AbstractDevice):
    """ A digital output pin. By default the off state means LOW output voltage level and the on state means HIGH voltage.
        However, if pin inverted=True then off == HIGH and on == LOW voltage levels."""
    def __init__(self, pin=None, id=None, name=None, state=0, state_names={0: "off", 1: "on"}):
        """
        :param pin: a DOutPin object.
        :param id: device ID string value only lower case latin letters and underscore sign are allowed.
        :param name: display name of device as Unicode str.
        :param state: initial state value.
        :param state_names: you can setup custom descriptions for each state value.
        """
        super(Relay, self).__init__(id=id, name=name)
        self.pin = pin
        self.state = state
        self.state_names = state_names
        self.switch_param = None

    def init_params(self, global_params_holder):
        super(Relay, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        self.switch_param = add_param(Parameter(type=ParameterType.switch_state, id='relay_{}_switch'.format(self.id),
                                                value=self.state))
