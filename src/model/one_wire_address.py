class OneWireAddress:
    ONE_WIRE_ADDRESS_BYTE_LENGTH = 8

    def __init__(self, one_wire_address: int=0):
        self.value = list(one_wire_address.to_bytes(self.ONE_WIRE_ADDRESS_BYTE_LENGTH, byteorder='big'))


ONE_WIRE_EMPTY_ADDRESS = OneWireAddress()
