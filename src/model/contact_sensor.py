from functools import partial

from model.device import AbstractDevice
from model.parameter import Parameter, ParameterType


class ContactSensor(AbstractDevice):
    """ Contact sensor is a device with a boolean read only state. """
    def __init__(self, id=None, name=None, state=0, state_names={0: "opened", 1: "closed"}):
        super(ContactSensor, self).__init__(id=id, name=name)
        self.state = state
        self.state_names = state_names
        self.state_param = None

    def init_params(self, global_params_holder):
        super(ContactSensor, self).init_params(global_params_holder)
        add_param = partial(self.add_param, global_params_holder=global_params_holder)

        self.state_param = add_param(Parameter(type=ParameterType.open_state, id='contact_sensor_{}_state'.format(self.id)))


class WallSwitch(ContactSensor):
    """ Wall switch to turn """
    def __init__(self, id=None, name=None, pin=None, inverted=False):
        super(WallSwitch, self).__init__(id=id, name=name, state_names={0: "off", 1: "on"})
        self.pin = pin
        self.state_param.type = ParameterType.switch_state
        self.inverted = inverted


class ReedSwitchSensor(ContactSensor):
    """ Reed switch sensor for doors or windows """
    def __init__(self, id=None, name=None, pin=None, normally_opened=True):
        super(ContactSensor, self).__init__(id=id, name=name)
        self.pin = pin
        self.normally_opened = normally_opened
