class IpAddress:

    def __init__(self, ip_str='0.0.0.0'):
        self.value = list(map(int, ip_str.split('.')))

    def __str__(self):
        return '.'.join(self.value)
