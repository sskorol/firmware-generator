from model.nodemcu import NodeMcu, DOutPin, Pinout, ReedSwitchSensor, ContactSensorPin
from model.relay import Relay
from model.ip_address import IpAddress
from model.parameter import ParameterHolder
from generator.nodemcu import NodeMcuFirmwareGenerator
from model.enums import WifiMode

pins = []
devices = []

# relays
dout_pin_relay1 = DOutPin('relay1', Pinout.D1, inverted=True)
dout_pin_relay2 = DOutPin('relay2', Pinout.D2, inverted=True)
dout_pin_relay3 = DOutPin('relay3', Pinout.D3, inverted=True)
dout_pin_relay4 = DOutPin('relay4', Pinout.D5, inverted=True)
dout_pin_relay5 = DOutPin('relay5', Pinout.D4, inverted=True)  # D4 is a led pin on nodeMCU v3 board
pins.extend([dout_pin_relay1, dout_pin_relay2, dout_pin_relay3, dout_pin_relay4, dout_pin_relay5])
reed_pin_1 = ContactSensorPin('reed1', Pinout.D6)
pins.append(reed_pin_1)
device_relay1 = Relay(dout_pin_relay1, 'relay1')
device_relay2 = Relay(dout_pin_relay2, 'relay2')
device_relay3 = Relay(dout_pin_relay3, 'relay3')
device_relay4 = Relay(dout_pin_relay4, 'relay4')
device_relay5 = Relay(dout_pin_relay5, 'relay5')
relays = [device_relay1, device_relay2, device_relay3, device_relay4, device_relay5]
devices.extend(relays)
reed_sensor_1 = ReedSwitchSensor('reed1', pin=reed_pin_1)
devices.append(reed_sensor_1)

# controller configuration
nodemcu1 = NodeMcu('nodemcu1')

# nodemcu1.wifi_mode = WifiMode.access_point
# nodemcu1.wifi_ap_ssid = 'test_wifi'
# nodemcu1.wifi_ap_password = 'test1234'
nodemcu1.wifi_mode = WifiMode.dhcp_client
nodemcu1.wifi_ap_ssid = 'test_wifi'
nodemcu1.wifi_ap_password = '12345678'
# nodemcu1.ip_address = IpAddress('192.168.43.100')
# nodemcu1.ip_gateway = IpAddress('192.168.43.1')
# nodemcu1.ip_mask = IpAddress('255.255.0.0')


# override controller default params
nodemcu1.eeprom_feature_enabled = False
nodemcu1.networking_feature_enabled = True
nodemcu1.serial_api_feature_enabled = True
nodemcu1.serial_logging_feature_enabled = True
nodemcu1.lock_heavy_operations = False
nodemcu1.serial_port_bodrate = 9600

nodemcu1.pins.extend(pins)
nodemcu1.devices.extend(devices)

# enumerate parameter IDs list
params_holder = ParameterHolder()
for device in nodemcu1.devices + [nodemcu1]:
    device.init_params(params_holder)

# list of params which should be stored in EEPROM

generator = NodeMcuFirmwareGenerator()
firmware = generator.generate_firmware(nodemcu1, './target/gerasimenko_relay_switch_controller.ino')
for line_number, line_text in enumerate(firmware.split('\n')):
    if 180 < line_number < 300:
        print(line_text)
