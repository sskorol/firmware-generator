from model.contact_sensor import WallSwitch
from model.generic_controller import OneWirePin
from model.ip_address import IpAddress
from model.light import PwmBasedLightSource, PwmLightChannel
from model.nodemcu import NodeMcu, PwmPin, ContactSensorPin, DOutPin
from model.one_wire import DS18B20Sensor
from model.relay import Relay
from model.parameter import Parameter, ParameterHolder
from generator.nodemcu import NodeMcuFirmwareGenerator
from model.rules_engine import TranslateParamValueRule

pins = []
devices = []

# lights
pwm_pin_red = PwmPin('red', 12)
pwm_pin_green = PwmPin('green', 13)
pwm_pin_blue = PwmPin('blue', 14)
pwm_pin_white = PwmPin('white', 15)
pins.extend([pwm_pin_red, pwm_pin_green, pwm_pin_blue, pwm_pin_white])

light_ceiling = PwmBasedLightSource('ceiling', 'Основное освещение', dimmable=True, use_fade_on_switch=True)
light_window_red = PwmLightChannel(pwm_pin_red, 'red')
light_window_green = PwmLightChannel(pwm_pin_green, 'green')
light_window_blue = PwmLightChannel(pwm_pin_blue, 'blue')
light_window = PwmBasedLightSource('window', 'Подсветка штор', dimmable=True, use_fade_on_switch=True,
                                   channels=[light_window_red, light_window_green, light_window_blue])
lights = [light_ceiling, light_window]
devices.extend(lights)

# switches
switch_pin_ceiling_door = ContactSensorPin('ceiling_light_door_switch', 16)
switch_pin_window_door = ContactSensorPin('window_light_door_switch', 4)
switch_pin_3 = ContactSensorPin('unused_0', 5)
switch_pin_4 = ContactSensorPin('unused_1', 0)
pins.extend([switch_pin_ceiling_door, switch_pin_window_door, switch_pin_3, switch_pin_4])

switch_ceiling_door = WallSwitch('ceiling_light_door_switch', 'Выключатель у двери', switch_pin_ceiling_door)
switch_window_door = WallSwitch('window_light_door_switch', 'Выключатель подстветки у двери', switch_pin_window_door)
switches = [switch_ceiling_door, switch_window_door]
devices.extend(switches)

# relays
dout_pin_power_source = DOutPin('power_source', 9)
pins.append(dout_pin_power_source)
relay_power_source = Relay(dout_pin_power_source, 'power_source', 'Включение блока питания', )
devices.append(relay_power_source)

# temperature sensors
one_wire_pin = OneWirePin(pin_number=10)
pins.append(one_wire_pin)
one_wire_sensor_1 = DS18B20Sensor(one_wire_pin, 'mosfet_block', 'Температура платы')
one_wire_sensor_2 = DS18B20Sensor(one_wire_pin, 'power_source_box', 'Температура блока питания')
one_wire_devices = [one_wire_sensor_1, one_wire_sensor_2]
devices.extend(one_wire_devices)

# controller configuration
nodemcu1 = NodeMcu('nodemcu1')

nodemcu1.ip_address = IpAddress('192.168.2.10')
nodemcu1.ip_gateway = IpAddress('192.168.2.1')
nodemcu1.ip_mask = IpAddress('255.255.255.0')

nodemcu1.pins.extend(pins)
nodemcu1.devices.extend(devices)

# enumerate parameter IDs list
params_holder = ParameterHolder()
for device in nodemcu1.devices + [nodemcu1]:
    device.init_params(params_holder)

# list params which should be stored in EEPROM


# rules
rule_ceiling_switch = TranslateParamValueRule(switch_ceiling_door.state_param, light_ceiling.switch_param)
rule_window_switch = TranslateParamValueRule(switch_ceiling_door.state_param, light_window.switch_param)

nodemcu1.rules_engine.rules.extend([rule_ceiling_switch, rule_window_switch])

generator = NodeMcuFirmwareGenerator()
firmware = generator.generate_firmware(nodemcu1, './target/nik_lights.ino')
for line_number, line_text in enumerate(firmware.split('\n')):
    if 180 < line_number < 300:
        print(line_text)
